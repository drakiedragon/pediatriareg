from django import forms
from .models import Registrarpac

class RegModelForm(forms.ModelForm):
    class Meta:
        model = Registrarpac
        campos = ['nombrePaciente','rutPaciente', 'edadIngreso','edadActual','fechaIngreso']
        fields = '__all__'


  #  def clean_rutPaciente(self):
   #     rut = self.cleaned_data.get('rutPaciente')
    #    rut_base, digito = rut.split()
