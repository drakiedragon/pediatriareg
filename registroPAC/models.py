from django.db import models

# Create your models here.

class Registrarpac(models.Model):
    rutPaciente = models.CharField(max_length=11,blank=True,null=False)
    nombrePaciente = models.CharField(max_length=80,blank=True,null=True)
    edadIngreso = models.IntegerField()
    edadActual = models.IntegerField()
    fechaIngreso = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombrePaciente

