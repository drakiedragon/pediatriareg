from django.shortcuts import render
from .forms import RegModelForm
from .models import Registrarpac

# Create your views here.
def inicio(request):
    form = RegModelForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        rut2 = form_data.get('rutPaciente')
        nombre2 = form_data.get('nombrePaciente')
        edadIngreso2 = form_data.get('edadIngreso')
        edadActual2 = form_data.get('edadActual')
        fechaIngreso2 = form_data.get('fechaIngreso')
        objeto = Registrarpac.objects.create(nombrePaciente=nombre2,rutPaciente=rut2,edadIngreso=edadIngreso2,edadActual=edadActual2,fechaIngreso=fechaIngreso2)

    contexto = {
        'formulario': form,
    }
    return render(request, 'registro_paciente.html', contexto)
