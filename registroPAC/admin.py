from django.contrib import admin
from .models import *
from .forms import RegModelForm

class AdminRegistrarPAC(admin.ModelAdmin):
    list_display = ['rutPaciente','nombrePaciente','edadIngreso','edadActual','fechaIngreso']
    form = RegModelForm
    list_display_links = ['rutPaciente']
    list_editable = ['nombrePaciente']
    list_filter = ['fechaIngreso']
    search_fields = ['rutPaciente','nombrePaciente']

    #class Meta:
     #   model = Registrarpac
# Register your models here.
admin.site.register(Registrarpac, AdminRegistrarPAC)

