from django import forms
from .models import Registrarficha

class RegModelForm(forms.ModelForm):
    class Meta:
        model = Registrarficha
        campos = ['rutPaciente','nombrePaciente','enfDetectadas','tratamiento','fechaEnfermedad','fechaTerminoTratamiento','resultadoTratamiento']
        fields = '__all__'