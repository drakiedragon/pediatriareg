from django.shortcuts import render
from .forms import RegModelForm
from .models import Registrarficha
# Create your views here.
def inicio_fichas(request):
    form = RegModelForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        rut2 = form_data.get('rutPaciente')
        nombre2 = form_data.get('nombrePaciente')
        enfDetectadas2 = form_data.get('enfDetectadas')
        tratamiento2 = form_data.get('tratamiento')
        fechaEnfermedad2 = form_data.get('fechaEnfermedad')
        fechaTerminoTratamiento2 = form_data.get('fechaTerminoTratamiento')
        resultadoTratamiento2 = form_data.get('resultadoTratamiento')
        objeto = Registrarficha.objects.create(rutPaciente=rut2,nombrePaciente=nombre2,enfDetectadas=enfDetectadas2,tratamiento=tratamiento2,fechaEnfermedad=fechaEnfermedad2,fechaTerminoTratamiento=fechaTerminoTratamiento2,resultadoTratamiento=resultadoTratamiento2)

    contexto = {
        'formulario_fichas': form,
    }
    return render(request, 'registro_fichas.html', contexto)
# Create your views here.

