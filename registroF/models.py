from django.db import models

# Create your models here.
class Registrarficha(models.Model):
    rutPaciente = models.CharField(max_length=11,blank=True,null=False)
    nombrePaciente = models.CharField(max_length=80,blank=True,null=True)
    enfDetectadas = models.CharField(max_length=80,blank=True,null=True)
    tratamiento = models.CharField(max_length=80,blank=True,null=True)
    fechaEnfermedad = models.DateField(auto_now_add=False,auto_now=False)
    fechaTerminoTratamiento = models.DateField(auto_now_add=False,auto_now=False)
    resultadoTratamiento = models.CharField(max_length=80,blank=True,null=True)

    def __str__(self):
        return self.nombrePaciente, self.enfDetectadas, self.tratamiento, self.resultadoTratamiento