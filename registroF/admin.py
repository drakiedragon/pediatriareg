from django.contrib import admin
from .models import *
from .forms import RegModelForm

class AdminRegistrarFicha(admin.ModelAdmin):
    list_display = ['rutPaciente','nombrePaciente','enfDetectadas','tratamiento','fechaEnfermedad','fechaTerminoTratamiento','resultadoTratamiento']
    form = RegModelForm
    list_display_links = ['rutPaciente']
    list_editable = ['enfDetectadas','tratamiento','resultadoTratamiento']
    list_filter = ['fechaEnfermedad']
    search_fields = ['rutPaciente','nombrePaciente']

    # class Meta:
    #   model = Registrarficha

# Register your models here.
admin.site.register(Registrarficha,AdminRegistrarFicha)